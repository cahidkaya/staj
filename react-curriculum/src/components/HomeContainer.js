import React, {Component} from 'react';

class HomeContainer extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className= "App-intro">
				<form className="form-postal-code">
					<input type="text"/>
					<button type="submit">
						Get Weather
					</button>
				</form>
			</div>
		);
	}
}

export default HomeContainer;
