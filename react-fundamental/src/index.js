/**
 * Created by cahidkaya on 7/13/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.js';
import './index.css';

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);