/**
 * Created by cahidkaya on 7/17/17.
 */
import React, {Component} from 'react';
import queryString from 'query-string';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import api from '../utils/api';
import PlayerPreview from './PlayerPreview';
import Loading from "./Loading";

function Profile(props) {
	var info = props.profile;
	return (
		<PlayerPreview avatar={info.avatar_url} username={info.login}>
			<ul className='space-list-items'>
				{info.name && <li>{info.name}</li>}
				{info.location && <li>{info.location}</li>}
				{info.company && <li>{info.company}</li>}
				<li>Followers: {info.followers}</li>
				<li>Following: {info.following}</li>
				<li>Public Repos: {info.public_repos}</li>
				{info.blog && <li><a href={info.blog}>{info.blog}</a></li>}
			</ul>
		</PlayerPreview>
	);
}

function Player(props) {
	return (
		<div>
			<h1 className="header" style={{fontweight: 'bold'}}>
				{props.label}
			</h1>
			<h3 style={{textAlign: 'center'}}> {props.score} </h3>
			<Profile profile={props.profile}>
				<p> Test </p>
			</Profile>
		</div>
	);
}

Player.propTypes = {
	label  : PropTypes.string.isRequired,
	score  : PropTypes.number.isRequired,
	profile: PropTypes.object.isRequired
};

class Results extends Component {
	constructor(props) {
		super(props);
		this.state = {
			winner : null,
			loser  : null,
			error  : null,
			loading: true

		};
	}

	componentDidMount() {
		const players = queryString.parse(this.props.location.search);
		api.battle([
			players.playerOne,
			players.playerTwo
		]).then(function (result) {
			if (result === null) {
				this.setState({
					error: "Check for username",
					loading: false
				});
			} else {
				this.setState({
						error  : null,
						loading: false,
						winner : result[0],
						loser  : result[1]
				});
			}
		}.bind(this));
	}

	render() {
		const winner = this.state.winner;
		const loser = this.state.loser;
		const error = this.state.error;
		const loading = this.state.loading;
		return (<div>
				{loading &&
				<Loading />}
				{error &&
				<div>
					{error}
					<Link to='/battle'>
						<br/>
						Reset
					</Link>
				</div>}
				{!loading && !error &&
				<div className="row">
					<Player
						label='Winner'
						score={winner.score}
						profile={winner.profile}
					/>
					<Player
						label="Loser"
						score={loser.score}
						profile={loser.profile}
					/>
				</div>}
			</div>
		);
	}
}

export default Results;
