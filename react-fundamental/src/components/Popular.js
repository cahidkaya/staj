/**
 * Created by cahidkaya on 7/13/17.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import api from '../utils/api';
import Loading from './Loading';
const SelectLanguage = (props) => {
	const languages = ['All', 'JavaScript', 'Ruby', 'Java', 'CSS', 'Python'];
	return (
		<ul className='languages'>
			{languages.map((lang) => {
				return (
					<li
						style={lang === props.selectedLanguage ? {color: '#d0021b'} : null}
						onClick={props.onSelect.bind(null, lang)}
						key={lang}>
						{lang}
					</li>
				);
			})}
		</ul>
	);
};

SelectLanguage.propTypes = {
	selectedLanguage: PropTypes.string.isRequired,
	onSelect        : PropTypes.func.isRequired,
};
function RepoGrid(props) {
	return (
		<ul className="popular-list">
			{props.repos.map((repos, index) => {
				return (
					<li key={repos.name} className="popular-item">
						<div className="popular-rank"> #{index + 1} </div>
						<ul className="space-list-items">
							<li>
								<img
									href={repos.html_url}
									className="avatar"
									src={repos.owner.avatar_url}
									alt={'Avatar for' + repos.owner.login}
								/>
							</li>
							<li><a href={repos.html_url}> {repos.name} </a></li>
							<li> @{repos.owner.login}</li>
							<li> {repos.stargazers_count} star</li>
						</ul>
					</li>
				);
			})}
		</ul>

	);
}

RepoGrid.propTypes = {
	repos: PropTypes.array.isRequired,
};

class Popular extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedLanguage: 'All',
			repos           : null
		};
		this.updateLanguage = this.updateLanguage.bind(this);
	}

	componentDidMount() {
		this.updateLanguage(this.state.selectedLanguage);
	}

	componentWillUnmount() {
		this.updateLanguage(null);
		console.log('Component Unmount');
	}

	updateLanguage(lang) {
		this.setState(() => {
			return {
				selectedLanguage: lang,
				repos           : null
			};
		});
		api.fetchPopularRepos(lang)
			.then((repos) => {
				this.setState(() => {
					return {
						repos: repos
					};
				});
			});
	}

	render() {
		return (
			<div>
				<SelectLanguage
					selectedLanguage={this.state.selectedLanguage}
					onSelect={this.updateLanguage}
				/>
				{!this.state.repos
					? <Loading />
					: <RepoGrid repos={this.state.repos}/>}
			</div>
		);
	}
}

export default Popular;
