// //
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import PlayerPreview from './PlayerPreview';

class PlayerInput extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: '',
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		const value = event.target.value;

		this.setState({username:value});
	}

	handleSubmit(event) {
		event.preventDefault();

		this.props.onSubmit(
			this.props.id,
			this.state.username
		);
	}

	render() {
		return (
			<form className="column" onSubmit={this.handleSubmit}>
				<label className="header" htmlFor="username">
					{this.props.label}
				</label>
				<input
					id="username"
					placeholder="gitHub username"
					type="text"
					autoComplete="off"
					value={this.state.username}
					onChange={this.handleChange}
				/>
				<button
					className="button"
					type="submit"
					disabled={!this.state.username}
				> Submit
				</button>
			</form>
		);
	}
}

PlayerInput.propTypes = {
	id      : PropTypes.string.isRequired,
	label   : PropTypes.string.isRequired,
	onSubmit: PropTypes.func.isRequired,
};

PlayerInput.defaultProps = {
	label: 'Username',
};

class Battle extends Component {
	constructor(props) {
		super(props);

		this.state = {
			playerOne     : '',
			playerTwo     : '',
			playerOneImage: null,
			playerTwoImage: null,
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleReset = this.handleReset.bind(this);

	}

	handleSubmit(id, username) {
		this.setState(function () {
			const newState = {};
			newState[id] = username;
			newState[id + 'Image'] = 'https://github.com/' + username + '.png?size=200';
			return newState;
		});
	}

	handleReset(id) {
		this.setState(function () {
			const newState = {};
			newState[id] = '';
			newState[id + 'Image'] = null;
			return newState;
		});
	}

	render() {
		const match = this.props.match;
		const playerOne = this.state.playerOne;
		const playerTwo = this.state.playerTwo;
		const playerOneImage = this.state.playerOneImage;
		const playerTwoImage = this.state.playerTwoImage;
		return (
			<div>
				<div className="row">
					{!playerOne &&
					<PlayerInput
						id='playerOne'
						label='Player One'
						onSubmit={this.handleSubmit}
					/>}
					{playerOneImage !== null &&
					<PlayerPreview
						avatar={playerOneImage}
						username={playerOne}
						onReset={this.handleReset}
					>
						<button className="reset" onClick={this.handleReset.bind(null, 'playerOne')}>
							Reset
						</button>
					</PlayerPreview>
					}

					{!playerTwo &&
					<PlayerInput
						id='playerTwo'
						label='Player Two'
						onSubmit={this.handleSubmit}
					/>}
					{playerTwoImage !== null &&
					<PlayerPreview
						avatar={playerTwoImage}
						username={playerTwo}
					>
						<button
							className="reset"
							onClick={this.handleReset.bind(null, 'playerTwo')}
						>
							Reset
						</button>
					</PlayerPreview>
					}
				</div>
				{playerOneImage && playerTwoImage &&
				<Link
					className="button"
					to={{
						pathname: match.url + '/results',
						search  : '?playerOne=' + playerOne + '&playerTwo=' + playerTwo
					}}>
					Battle
				</Link>
				}
			</div>
		);
	}
}

export default Battle;
