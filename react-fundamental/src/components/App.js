/**     Created by cahidkaya on 7/13/17.     */
import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Popular from './Popular';
import Nav from './Nav';
import Home from './Home';
import Battle from './Battle';
import Results from './Results';
class App extends Component {
	render() {
		return (
			<Router>
				<div className="container">
					<Nav/>
					<Switch>
						<Route exact path="/popular" component={Popular}/>
						<Route exact path="/" component={Home}/>
						<Route exact path="/battle" component={Battle}/>
						<Route path="/battle/results" component={Results}/>
						<Route render={() => {
							return (<p>404 Not Fount</p>);
						}}/>
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
