/**
 * Created by cahidkaya on 7/14/17.
 */
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
class Home extends Component {
    render() {
        return (
            <div className="home-conteiner" >
                <h1>GitHub Battle: Battle with your friends...</h1>
                <Link className="button" to='battle'>
                    Battle
                </Link>
            </div>
        );
    }
}
export default Home;