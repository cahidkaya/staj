/**
 * Created by cahidkaya on 7/10/17.
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
});