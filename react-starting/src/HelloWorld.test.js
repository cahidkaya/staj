/**
 * Created by cahidkaya on 7/10/17.
 */
import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import HelloWorld from './HelloWorld';

describe(HelloWorld, () => {
    const name = 'Person';
    const mockRemoveGreeting = jest.fn();
    const component = shallow(
        <HelloWorld name = {name} removeGreeting={mockRemoveGreeting}/>
    );

});
