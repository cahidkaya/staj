/**
 * Created by cahidkaya on 7/10/17.
 */
import React, { Component } from 'react';
import HelloWorld from './HelloWorld';
import AddGreeter from './AddGreeter';
class HelloWorldList extends Component {
    constructor(props) {
        super(props);
        this.state = {greeting : ['Jim', 'Sally', 'Bender', 'Flask']};
        this.addGreeting = this.addGreeting.bind(this);
        this.removeGreeting = this.removeGreeting.bind(this);
    }
    renderGreetings() {
        return this.state.greeting.map(name => (
            <HelloWorld
                key={name}
                name = {name}
                removeGreeting = {this.removeGreeting}
            />
        ));
    }
    addGreeting(newName)  {
      this.setState({greeting: [...this.state.greeting, newName] });
    }
    removeGreeting(removeName)  {
        const filteredGreetings = this.state.greeting.filter(name => {
            return name !== removeName;
        });
        this.setState({greeting:filteredGreetings});
    }
    render() {
        return (
        <div className="HelloWorldList">
            <AddGreeter addGreeting={this.addGreeting}/>
            {this.renderGreetings()}
        </div>
    );
    }
}

export default HelloWorldList;
