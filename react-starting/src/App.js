import React from 'react';
import './HelloWorld.css';
import HelloWorldList from './HelloWorldList.js';

const App = () => {
    return (
        <div className="App">
            <HelloWorldList/>
        </div>
    );
};
export default App;